/* fact.c by hkob */

/* x の階乗を計算して返す */
int fact(int x) {
    return x < 0 ? -1 : x < 2 ? 1 : x * fact(x-1);
}
